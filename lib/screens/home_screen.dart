import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:my_cafef/models/news_list.dart';
import 'package:my_cafef/widgets/news_tile.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    TextEditingController _searchTextController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 4,
        leading: Image(
          alignment: Alignment.topLeft,
          fit: BoxFit.fitHeight,
          image: AssetImage("assets/logo.png"),
        ),
        leadingWidth: 150,
        centerTitle: false,
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: AnimSearchBar(
                width: 180,
                color: Colors.grey[200],
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.black54,
                ),
                rtl: true,
                textController: _searchTextController,
                onSuffixTap: () {
                  _searchTextController.clear();
                }),
          ),
          SizedBox(
            width: 5,
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.tv),
            color: Colors.black54,
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.monetization_on_sharp),
            color: Colors.black54,
          ),
        ],
      ),
      body: Center(
        child: ListView.builder(
          itemCount: newsList.length,
          itemBuilder: (context, index) {
            return NewsTile(news: newsList[index]);
          },
        ),
      ),
      backgroundColor: Colors.grey,
    );
  }
}
