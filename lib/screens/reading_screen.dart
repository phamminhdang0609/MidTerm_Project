import 'package:flutter/material.dart';
import 'package:my_cafef/models/news.dart';

class ReadingScreen extends StatelessWidget {
  const ReadingScreen({
    Key? key,
    required this.news,
  }) : super(key: key);
  final News news;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(
                  image: NetworkImage(news.imageUrl),
                  height: 250,
                  width: double.infinity,
                  fit: BoxFit.fitWidth,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      news.category,
                      style: TextStyle(
                          color: Colors.blue[600],
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Row(
                      children: [
                        Icon(Icons.timer_outlined),
                        SizedBox(
                          width: 5,
                        ),
                        Text(news.postTime),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  news.title,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                Divider(
                  thickness: 3,
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: Container(
                    child: Text(
                      news.detail,
                      overflow: TextOverflow.visible,
                      softWrap: true,
                      style: TextStyle(fontSize: 17),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
