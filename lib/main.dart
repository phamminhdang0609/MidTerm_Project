import 'package:flutter/material.dart';
import 'package:my_cafef/models/news_list.dart';
import 'package:my_cafef/screens/home_screen.dart';
import 'package:my_cafef/screens/login_screen.dart';
import 'package:my_cafef/screens/reading_screen.dart';
import 'package:my_cafef/screens/splashing_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashingScreen(),
    );
  }
}
