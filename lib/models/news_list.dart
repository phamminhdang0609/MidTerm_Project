import 'package:my_cafef/models/news.dart';

final List<News> newsList = [
  News(
    id: 1,
    title:
        "Vì đâu vừa mới đi vào khai thác, doanh nghiệp quản lý đường sắt Cát Linh - Hà Đông đã có khoản lỗ lũy kế 160 tỷ đồng?",
    imageUrl:
        "https://cafefcdn.com/thumb_w/650/203337114487263232/2022/6/19/photo1655579272413-16555792727711274059055.jpeg",
    category: "Pháp luật & Lao động",
    detail:
        "Từ khi hoạt động vào tháng 6/2015 đến hết năm 2021, Hanoi Metro lỗ lũy kế tổng cộng 160 tỷ đồng. Như vậy lỗ từ năm 2020 trở về trước - khoảng thời gian chưa vận hành thương mại tàu - vào khoảng 100 tỷ đồng. Giai đoạn này doanh thu bằng 0 trong khi công ty vẫn phải duy trì đội ngũ đội ngũ nhân viên do đó việc ghi nhận kết quả âm là bình thường.\nDự án hoạt động chính thức từ 6/11/2021, song có 15 ngày miễn phí đầu tiên cho người dân trải nghiệm. Có nghĩa trong năm tài chính 2021, Hanoi Metro ghi nhận doanh thu từ việc bán vé tàu điện chỉ trong khoảng hơn 1 tháng cuối năm, từ 21/11 đến 31/12/2021. Trong khi đó các chi phí vẫn phát sinh cả năm, đặc biệt là nhiều khoản chi phí liên quan đến việc đưa vào khai thác, vận hành dẫn đến mức lỗ 2021 cao hơn nhiều các năm trước.\n Mặt khác, đơn vị kiểm toán chỉ ra Hanoi Metro chưa chính thức được UBND TP. Hà Nội bàn giao tài sản từ dự án ga đường sắt đô thị Hà Nội số 2A , tuyến Cát Linh - Hà Đông do đó chi phí khấu hao tương ứng chưa được tính cho kết quả kinh doanh năm 2021. Mặt khác, công ty chưa xây dựng được đơn giá đặt hàng dịch vụ thực hiện và được UBND TP. Hà Nội phê duyệt, ký hợp đồng. Do vậy chưa thể xác định được ảnh hưởng của vấn đề này đến báo cáo tài chính năm 2021.\nVề phía Hanoi Metro, Ban Tổng giám đốc doanh nghiệp này lý giải kết quả kinh doanh bị tác động bởi Covid-19 nhưng không thể ước tính được một cách hợp lý các ảnh hưởng đến báo cáo tài chính.\nNăm 2022, Hanoi Metro đặt mục tiêu doanh thu 476 tỷ đồng, cao gấp gần 90 lần so với cùng kỳ, mục tiêu lợi nhuận trước thuế hơn 17 tỷ đồng và nếu hoàn thành sẽ là năm đầu tiên đơn vị này ghi nhận lãi.",
    postTime: "7 giờ trước",
  ),
  News(
    id: 1,
    title:
        "WSJ: Chứng khoán Mỹ bước vào thị trường giá xuống nhưng vẫn chưa chạm đáy, tất cả phụ thuộc vào Fed!",
    imageUrl:
        "https://cafefcdn.com/thumb_w/650/203337114487263232/2022/6/21/photo1655801818122-16558018181982009948347.jpg",
    category: "Pháp luật & Lao động",
    detail:
        "Nhà đầu tư thường nói rằng Fed chính là nguyên nhân khiến thị trường biến động. Đó là bởi trước đây, Fed cũng là nhân tố xoay chuyển thị trường. Nhìn lại năm 1950, S&P 500 giảm ít nhất 15% trong 17 phiên, theo nghiên cứu của Vickie Chang – chiến lược gia thị trường tại Goldman Sachs. Trong 11/17 lần như vậy, thị trường chỉ chạm đáy khi Fed nới lỏng chính sách tiền tệ.\nCó thể thấy, \"đích đến đáy\" có thể là một hành trình đau đớn cho nhà đầu tư. S&P 500 đã giảm 23% trong năm 2022, khiến đây là khởi đầu tồi tệ nhất trong 1 năm kể từ năm 1932. Chỉ số này đã giảm 5,8% vào tuần trước và ghi nhận mức giảm lớn nhất từ đợt bán tháo khi đại dịch bắt đầu bùng phát.\nQuá trình thắt chặt chính sách của Fed mới chỉ bắt đầu. Sau khi tăng lãi suất ở mức cao nhất kể từ năm 1994 vào thứ Tư tuần trước, NHTW Mỹ phát tín hiệu sẽ thực hiện 1 đợt nâng nữa trong năm nay để kiềm chế lạm phát.\nChính sách tiền tệ được thắt chặt cùng với lạm phát ở mức cao nhất trong 1 thập kỷ đã khiến nhiều nhà đầu tư lo ngại về suy thoái kinh tế. Số liệu kinh tế, tâm lý người tiêu dùng, hoạt động xây dựng nhà và nhà máy đều suy yếu đáng kể trong những tuần gần đây. Trong khi lợi nhuận của các doanh nghiệp tăng mạnh, giới phân tích cho rằng họ sẽ phải chịu áp lực trong nửa cuối năm nay.\nTheo FactSet, 417 doanh nghiệp trong S&P 500 đề cập đến lạm phát trong báo cáo tài chính quý I, con số cao nhất kể từ năm 2010. Trong những tuần tới, nhà đầu tư sẽ chờ đợi các số liệu bao gồm doanh số bán nhà hiện có, tâm lý người tiêu dùng và doanh số bán nhà mới để đánh giá tình trạng của nền kinh tế.\nDavid Donabedian – CIO của CIBC Private Wealth, cho biết: \"Tôi không cho rằng xu hướng sụt giảm của thị trường sẽ tiếp tục với tốc độ này. Nhưng ý tưởng về việc thị trường đang chuẩn bị chạm đáy khó có thể xác định.\"\nDonabedian nói rằng ông không khuyến nghị khách hàng bắt đáy hay mua cổ phiếu giá rẻ với kỳ vọng rằng thị trường sẽ sớm \"quay đầu\". Ông cho hay, ngay cả với đợt bán tháo mạnh, giá cổ phiếu Phố Wall vẫn không hề rẻ. Ngoài ra, dự báo lợi nhuận dường như vẫn khá lạc quan trong tương lai.\nTheo FactSet, chỉ số S&P 500 đang giao dịch ở mức 15,4 lần lợi nhuận dự phóng 12 tháng, chỉ thấp hơn 1 chút so với mức trung bình 15 năm là 15,7. Các nhà phân tích hiện vẫn dự báo các công ty trong S&P 500 ghi nhận tăng trưởng lợi nhuận theo tỷ lệ phần trăm là 2 con số trong quý III và IV.",
    postTime: "2 giờ trước",
  ),
  News(
    id: 1,
    title: "Một vùng từ cách đây hơn chục năm đã có nhiều ô tô hơn cả điều hoà",
    imageUrl:
        "https://cafefcdn.com/thumb_w/650/203337114487263232/2022/6/21/photo1655770936141-16557709362601555912939.jpg",
    category: "Pháp luật & Lao động",
    detail:
        "Trong khi rõ ràng, Tây Nguyên không phải là vùng có thu nhập cao nếu so với các vùng kinh tế khác và nếu chỉ đứng thứ 4 về tỷ lệ sở hữu ô tô. Vậy lý do cho điểm đặc biệt này là gì?\nLý do có thể đến từ đặc thù về khí hậu.\nTây Nguyên nằm trong vùng Nhiệt đới Xavan, gồm nhiều tiểu vùng nhưng khí hậu phổ biến là nhiệt đới gió mùa cao nguyên. Khí hậu ở đây được chia thành hai mùa rõ rệt :\n\tMùa khô: Từ tháng 11 năm trước đến tháng 4 năm sau, khí hậu khô và lạnh, độ ẩm thấp.\n\tMùa mưa: Từ tháng 5 đến tháng 10, khí hậu ẩm và dịu mát. Trong suốt mùa mưa, những cơn mưa xối xả có thể gây ra lũ quét , đường lầy lội làm khó khăn trong việc di chuyển. Trong suốt tháng 7 và đầu tháng 8 mưa dường như có thể kéo dài liên tục.\nNhiệt độ trung bình năm là 24oC trong đó tháng 3 và tháng 4 là hai tháng nóng và khô nhất. Lượng mưa trung bình năm khoảng 1.900 mm - 2.000 mm, tập trung chủ yếu vào mùa mưa.\nDo sự khác biệt về độ cao, khí hậu ở cao nguyên nên khi đạt độ cao khoảng 500m sẽ tương đối mát mẻ và có mưa, ở vị trí cao hơn như cao nguyên với độ cao 1000m là thời tiết Đà Lạt thì mát mẻ quanh năm, đặc trưng của vùng có khí hậu núi cao. Trung Tây Nguyên như thời tiết Đắk Lắk và thời tiết Đắk Nông thì có độ cao thấp hơn và nền nhiệt độ cao hơn hai tiểu vùng phía Bắc và Nam.\nBáo cáo Khảo sát mức sống dân cư 2020 cũng chỉ ra rằng, các hộ gia đình trong ngành dịch vụ khác có số ô tô tính trên 100 hộ cao nhất trong số các nhóm ngành. Cứ 100 hộ trong ngành này thì có 9,9 chiếc ô tô. Ngược lại, các hộ nông nghiệp có số ô tô trên 100 hộ thấp nhất. 100 hộ làm nông nghiệp mới có 1 chiếc ô tô duy nhất.",
    postTime: "2 giờ trước",
  ),
];
