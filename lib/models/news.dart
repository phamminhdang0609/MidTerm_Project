class News {
  final int id;
  final String title;
  final String category;
  final String postTime;
  final String detail;
  final String imageUrl;

  News({
    required this.id,
    required this.title,
    required this.imageUrl,
    required this.category,
    required this.detail,
    required this.postTime,
  });
}
